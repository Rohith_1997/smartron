var mongoClient = require('mongodb').MongoClient;
var redis = require('redis');
var client = redis.createClient();
ObjectID = require('mongodb').ObjectID;
var cfg = require('./config');

var objId;
var events = require('events');
var eventEmitter = new events.EventEmitter();
var date = new Date();
var sec = date.getSeconds();
console.log(sec);
function createAlarm(obj,callback){
  var objId;
  console.log(JSON.stringify(obj));
  mongoClient.connect(cfg.mongodb.hostname+':'+cfg.mongodb.port+'/alarms',function(err,db){
    var collection = db.collection('alarms');
    collection.insert(obj,function(err,reply){
      if(err){
        callback(1);
      }
      objId = obj._id;
      callback(objId);
    });
  });
}

module.exports.create = createAlarm;

function update(id,obj,callback){
  mongoClient.connect(cfg.mongodb.hostname+':'+cfg.mongodb.port+'/'+'alarms',function(err,db){
    var collection = db.collection('alarms');
    var objId = new ObjectID(id);
    collection.findOne({_id:objId},function(err,reply){
      if(err){
        callback(1);
      }
      else{
        callback(0);
        var object = {
          "time" : reply.time,
          "function" :reply.function
        }
        if(obj.time != undefined){
          object.time = obj.time;
        }
        if(obj.function != undefined){
          object.function = obj.function;
        }
        var string = JSON.stringify(object);
        client.hset('alarms',id,string,function(err, reply) {
          console.log(reply);
        });
        if(obj.time != undefined){
          collection.update({_id:objId},{$set:{"time":obj.time}});
        }
        if(obj.status != undefined){
          collection.update({_id:objId},{$set:{"status":obj.type}});
        }
        if(obj.label != undefined){
          collection.update({_id:objId},{$set:{"label":obj.label}});
        }

      }
    })

  })
}
module.exports.updateAlarm = update;
function set(id,callback){
  var objId = new ObjectID(id);
  function afterConnect(err,db){
    var collection = db.collection('alarms');
    console.log(id);
    collection.update({_id:objId},{$set:{status:"on"}})
    collection.findOne({_id:objId},function (err,docs){
      if(err){
        callback(1);
      }
      else{
        // console.log("docs: "+docs);
        console.log("id: "+id);
        var json = {
          'time':docs.time,
          'device':docs.device,
          'type':docs.type
        }
        var string = JSON.stringify(json);
        client.hset('alarms',id,string,function(err, reply) {
          console.log(reply);
        });
      }
  });
}
  mongoClient.connect(cfg.mongodb.hostname+':'+cfg.mongodb.port+'/alarms',afterConnect);
}
module.exports.setAlarm = set;
function get(id,callback){
  var objId = new ObjectID(id);
  mongoClient.connect(cfg.mongodb.hostname+':'+cfg.mongodb.port+'/alarms',function(err,db){
    var collection = db.collection('alarms');
    if(id != undefined ){
      collection.findOne({_id:objId},function (err,docs){
        if(err){
          callback(1);
        }
        else{
          callback(docs);
        }

      })
    }
    else{
      collection.find().toArray(function(err,results){
        console.log(results);
        callback(results);
      })
    }

 });
}
module.exports.getAlarm = get;
function getUser(user,callback){
  mongoClient.connect(cfg.mongodb.hostname+':'+cfg.mongodb.port+'/alarms',function(err,db){
    var collection = db.collection('alarms');
    collection.find({"user":user}).toArray(function(err,results){
      callback(results);
    })
  })
}
module.exports.getUser = getUser
function deleteAlarm(id,callback){
  console.log(id);
  var objId = new ObjectID(id);
  var time;
  function afterConnect(err,db){
    var collection = db.collection('alarms');

    collection.findOne({_id:objId},function (err,docs){
      if(err){
        callback(1);
      }
      client.hdel('alarms',id);
      collection.deleteOne({_id:objId});
    });
  }
  mongoClient.connect(cfg.mongodb.hostname+':'+cfg.mongodb.port+'/alarms',afterConnect);
}
module.exports.deleteAlarm = deleteAlarm;
