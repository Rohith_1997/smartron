var express = require('express');
var url = require('url');
var app = express();
var bodyParser = require('body-parser');
var alarm = require('./alarmOther');
var mongoClient = require('mongodb').MongoClient;
var redis = require('redis');
var client = redis.createClient();
ObjectID = require('mongodb').ObjectID;
var cfg = require('./config');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
function valid(time,callback){
  var intTime = parseInt(time);
  if(time === ""){
    callback(1);
  }
  else if(typeof(time) != 'string'){
    callback(1);
  }
  else if(typeof(time) == 'string'){
    var valid = isNaN(time);
    if(valid){
      callback(1);
    }
    else if(intTime<0 || intTime>2400){
      callback(1);
    }
    else {
      callback(0);
    }
  }
}
function validId(id,callback){
  var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
  if(!checkForHexRegExp.test(id)){
    callback(1);
  }
  else{
    callback(0);
  }
}
app.get('/createAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.put('/createAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.post('/createAlarm',function(req,res){
  var obj = {
    time : req.body.time,
    label: req.body.label,
    user:req.body.user,
    mode:req.body.mode,
    function:req.body.function,
    status : "off"
  };
  console.log(req.body);
  valid(obj.time,function(response){
    if(response === 1){
      res.status(422);
      res.end("invalid time format");
      console.log("invalid time format");
    }
    else{
      alarm.create(obj,function(response){
        var id = {
          id:response
        };
        if(response === '1'){
          res.status(422);
          res.end("error");
          console.log("error");
        }
        else{
          res.send(id);
          res.status(201);
          res.end("Success");
          console.log("Success");
        }
    });
  }
})
});
/**
*@api {post} /createAlarm Request user to create alarm
*@apiName Alarm
*@apiGroup Alarm
*@apiDescription It creates a new alarm
*@apiParam {String} time* time to feed alarm .
*@apiParam {String} label* label to feed alarm.
*@apiParam {String} status* status to feed alarm.
*@apiParam {String} user* username
*@apiParam {String} mode* private or public
*@apiParam {String} function *function to be executed by the alarm
*/
app.get('/updateAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.post('/updateAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})

app.put('/updateAlarm',function(req,res){
  var obj = {
    id :req.body.id,
    time : req.body.time,
    label : req.body.label,
    status : req.body.status,
    function : req.body.function,
    user : req.body.user,
    mode : req.body.mode
  }
  validId(obj.id,function(response){
    if(response === 1){
      res.status(422);
      res.end("invalid id");
    }
    else if(response === 0){
      valid(obj.time,function(response){
        if(response === 1){
          res.status(422);
          res.end("invalid time");
        }
        else if(response === 0){
          alarm.updateAlarm(obj.id,obj,function(response){
            if(response === 0){
              res.status(202);
              res.end("Success");
            }
            else if (response === 1){
              res.status(422);
              res.end("invalid id");
            }
          });
        }
      })
    }
  });
});
/**
*@api {put} /updateAlarm Request user to update
*@apiGroup Alarm
*@apiDescription this is used to update the alarm
*@apiParam {String} id* id of the object created in mongodb
*@apiParam {String} time* time to feed alarm
*@apiParam {String} label* label to feed alarm
*@apiParam {String} status* status to feed alarm
*@apiParam {String} user* username
*@apiParam {String} mode* private or public
*@apiParam {String} function* function of the alarm
*/
app.get('/setAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.post('/setAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.put('/setAlarm',function(req,res){
  var id = req.body.id;
  validId(id,function(response){
    if(response === 1){
      res.status(422);
      res.end("invalid Id");
    }
    else if(response === 0){
      alarm.setAlarm(id,function(response){
      });
      res.status(202);
      res.end("sucess");
    }
  })
})
/**
*@api {put} /setAlarm Request user to set Alarm
*@apiGroup Alarm
*@apiDescription this is used to set Alarm
*@apiParam {String} id*id of the object
*/
app.post('/getAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.put('/getAlarm',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.get('/getAlarm',function(req,res){
  var id = req.query.id;
  if (id === undefined){
    alarm.getAlarm(id,function(response){
    res.send(response);
    })
  }
  else{
    console.log(id);
    validId(id,function(response){
      if(response === 1){
        res.status(422);
        res.send("invalid Id")
      }
      else if(response === 0){

        alarm.getAlarm(id,function(response){
          res.send(response);
          res.status(202);
          res.end();
          });
      }
    })
  }
})
/**
*@api {get} /getAlarm?id=value Print the alarms
*@apiGroup Alarm
*@apiDescription this is used to display a specific alarm or all the alarms stored in mongodb
*@apiParam{String} id*id to uniquely idnetify the alarm
*/
app.put('/getAlarmUser',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.post('/getAlarmUser',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.get('/getAlarmUser',function(req,res){
  var user = req.query.user;
  alarm.getUser(user,function(response){
    if(response == null){
      res.status(422);
      res.send("no user exsists");
    }
    else{
      res.status(202);
      res.send(response);
      res.end();
    }
  })
})
/**
*@api {get} /getAlarmUser?user=value print the alarms with the specific user
*@apiGroup Alarm
*@apiDescription this is used to display the alarms specific to the given user
*@apiParam{String} user *user to uniquely identify the alarm.
*/
app.put('/delete',function(req,res){
  res.status(405);
  res.send("invalid method");
})
app.post('/delete',function(req,res){
  res.status(405);
  res.send("invalid method");
})

app.delete('/delete',function(req,res){
  var id = req.query.id;
  validId(id,function(response){
    if(response === 1){
      res.status(422);
      res.send("invalid Id");
      res.end();
    }
    else {
      console.log("entered");
      alarm.deleteAlarm(id,function(response){
          console.log("sucess");

      });
      res.status(200);
      res.end("Success");
    }
  })
})
/**
*@api {delete} /delete?id=value delete alarms
*@apiGroup Alarm
*@apiDescription this is used delete a specific alarm.
*@apiParam{String} id*id to uniquely identify the alarm
*/
var server = app.listen(cfg.app.port,function(){
console.log("Alarm app running on port",cfg.app.port);});
