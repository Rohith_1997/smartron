var request = require('request');
var child_process = require('child_process');

const firstEntityValue = (entities, entity) => {
  const val = entities && entities[entity] &&
    Array.isArray(entities[entity]) &&
    entities[entity].length > 0 &&
    entities[entity][0].value
  ;
  if (!val) {
    return null;
  }
  return typeof val === 'object' ? val.value : val;
};
function runCmd(cmd){
  var resp = child_process.execSync(cmd);
  var result = resp.toString('UTF8');
  return result;
}

exports.alarm = function({context,entities}) {
  var bod;
  return new Promise(function (resolve, reject) {
    var curlCmd = 'curl -sS'
    var url = ' http://10.11.14.94:8080/getAlarm'
    var cmd = curlCmd + url
    console.log("entities: "+JSON.stringify(entities)+"\n");
    console.log("context-in: "+JSON.stringify(context)+"\n");
    var alarmType = firstEntityValue(entities,'alarm_type');
    console.log("alarmType: "+alarmType);
    var time = firstEntityValue(entities,'datetime');
    if(alarmType === "get"){
      var result = runCmd(cmd);
      console.log("ro"+result);
      var res = JSON.parse(result);
      console.log("rohith"+res);
      delete context.missingTime;
      delete context.alarmType;
      context.Display= res;
      delete context.date_str;
    }
    else{
      if(alarmType){
        context.alarmType = alarmType;
      }
      if(time){
      var requestData = {
        "time" : time,
        "label":"ro"
      }
      console.log(requestData);
      request({
        url: 'http://10.11.14.94:8080/createAlarm',
        method: "POST",
        json: true,
        headers: {
            "content-type": "application/json",
        },
        body: requestData
      },
      function(error, response, body){
        var id = {
          "id":body
        }
        request({
          url:'http://10.11.14.94:8080/setAlarm',
          method :"PUT",
          json:true,
          headers:{
            "content-type": "application/json",
          },
          body:id
        })
      })
        context.date_str = time;
        delete context.missingTime;
      }
      else{
        context.missingTime = true;
        delete context.datetime;
      }
    }
    console.log("context-out: "+JSON.stringify(context));
    return resolve(context);
  })
}
