define({ "api": [
  {
    "type": "get",
    "url": "/getClimate?place=value&time&temp&wind&des",
    "title": "Request user to get weather update",
    "name": "Weather",
    "group": "Weather",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "time",
            "description": "<p>time of sunrise and sunset</p>"
          },
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "temp",
            "description": "<p>temp of current weather</p>"
          },
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "wind",
            "description": "<p>wind conditions of the weather</p>"
          },
          {
            "group": "Parameter",
            "type": "null",
            "optional": false,
            "field": "des",
            "description": "<p>description of weather</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./weather.js",
    "groupTitle": "Weather"
  }
] });
