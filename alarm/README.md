# Reminder

Alarm is an appication used to create, set, update, view and delete reminders.
* Descrpition
*  Usage
*  Installation

### Description
- The application is used to keep alarms with respect to time. The alarms ring at the set time.


### Usage

- A server is created through which the one can use the app.
- The create operation is done by giving a JSON input using POST method
    {
    "time":"hhmm format",
    "label":"task if required"
}
- The update operation can be done similarly by pushing a JSON object with the updated fields and the Id of the alarm that is to be updated using PUT method.
- The delete operation is done by mentioning the id of the alarm that is to be deleted by GET method.
- getAlarm operation is done by mentionning the id of the alarm that is to be viewed, else all the alarms in the database can be viewed.



### Installation
- Requires [Node.js](https://nodejs.org/) v4+ to run.
- Install the dependecies and devDependencies and start the server.


```sh
$ cd alarm
$ npm install -d
$ node app.js
```
