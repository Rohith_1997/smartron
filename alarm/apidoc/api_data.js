define({ "api": [
  {
    "type": "post",
    "url": "/createAlarm",
    "title": "Request user to create alarm",
    "name": "Alarm",
    "group": "Alarm",
    "description": "<p>It creates a new alarm</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "time",
            "description": "<ul> <li>time to feed alarm .</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<ul> <li>label to feed alarm.</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<ul> <li>status to feed alarm.</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<ul> <li>username</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mode",
            "description": "<ul> <li>private or public</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "function",
            "description": "<p>*function to be executed by the alarm</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./moreAlarm.js",
    "groupTitle": "Alarm"
  },
  {
    "type": "delete",
    "url": "/delete?id=value",
    "title": "delete alarms",
    "group": "Alarm",
    "description": "<p>this is used delete a specific alarm.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>*id to uniquely identify the alarm</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./moreAlarm.js",
    "groupTitle": "Alarm",
    "name": "DeleteDeleteIdValue"
  },
  {
    "type": "get",
    "url": "/getAlarm?id=value",
    "title": "Print the alarms",
    "group": "Alarm",
    "description": "<p>this is used to display a specific alarm or all the alarms stored in mongodb</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>*id to uniquely idnetify the alarm</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./moreAlarm.js",
    "groupTitle": "Alarm",
    "name": "GetGetalarmIdValue"
  },
  {
    "type": "get",
    "url": "/getAlarmUser?user=value",
    "title": "print the alarms with the specific user",
    "group": "Alarm",
    "description": "<p>this is used to display the alarms specific to the given user</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p>*user to uniquely identify the alarm.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./moreAlarm.js",
    "groupTitle": "Alarm",
    "name": "GetGetalarmuserUserValue"
  },
  {
    "type": "put",
    "url": "/setAlarm",
    "title": "Request user to set Alarm",
    "group": "Alarm",
    "description": "<p>this is used to set Alarm</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>*id of the object</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./moreAlarm.js",
    "groupTitle": "Alarm",
    "name": "PutSetalarm"
  },
  {
    "type": "put",
    "url": "/updateAlarm",
    "title": "Request user to update",
    "group": "Alarm",
    "description": "<p>this is used to update the alarm</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<ul> <li>id of the object created in mongodb</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "time",
            "description": "<ul> <li>time to feed alarm</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<ul> <li>label to feed alarm</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<ul> <li>status to feed alarm</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<ul> <li>username</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mode",
            "description": "<ul> <li>private or public</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "function",
            "description": "<ul> <li>function of the alarm</li> </ul>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./moreAlarm.js",
    "groupTitle": "Alarm",
    "name": "PutUpdatealarm"
  }
] });
