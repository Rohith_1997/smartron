var config = module.exports = {};

config.env = 'global';

//Application
config.app = {};
config.app.hostname = 'localhost';
config.app.port = '8082';

//Redis database
config.redis = {};
config.redis.hostname = 'localhost';
config.redis.port = '6379';

config.mongodb = {};
config.mongodb.hostname = 'mongodb://localhost';
config.mongodb.port = '27017';
